# https://www.youtube.com/watch?v=C6jJg9Zan7w&t=2297s

import turtle
import time 

t = 6       #tid 1. på 4 sekunder
t2 = 2      #tid 2. på 2 sekunder

     

n = 1    # n står för nuddningar. Efter 3 nuddningar så blir det "SPEED UP"  

wn = turtle.getscreen()
wn.title("pong game")                     
wn.bgcolor("black")                      #Själva skärmen... turtle.getscreen()
wn.setup(width=800, height=600)
wn.tracer(0)

score_a = 0
score_b = 0


startF = turtle.Turtle()
startF.direction = "inte redo"

paddle_a = turtle.Turtle()
paddle_a.speed(0)
paddle_a.shape("square")                            
paddle_a.color("blue")
paddle_a.shapesize(stretch_wid=5, stretch_len=1)
paddle_a.penup()                                            #Paddan a.... turtle.Turtle()
paddle_a.goto(-360, 0)

paddle_b = turtle.Turtle()
paddle_b.speed(0)
paddle_b.shape("square")                                 #Paddan b... turtle.Turtle()
paddle_b.color("red")
paddle_b.shapesize(stretch_wid=5, stretch_len=1)
paddle_b.penup()
paddle_b.goto(360, 0)

penst = turtle.Turtle()
penst.speed(0)
penst.color("white")
penst.penup()             #Första pennan... turtle.Turtle()
penst.hideturtle()
penst.goto(0, 220)
penst.write("Pong Game", align ="center", font=("courier", 50, "normal"))  

penk = turtle.Turtle()
penk.speed(0)
penk.color("white")
penk.penup()             #Första pennan... turtle.Turtle()
penk.hideturtle()
penk.goto(0, -100)
penk.write("Press F to continue", align ="center", font=("courier", 30, "normal"))  

ball = turtle.Turtle()
ball.speed(0)
ball.shape("square")
ball.color("white")                
ball.penup()                    #Bollen... turtle.Turtle()
ball.goto(0, 0)
ball.dx = 0.1
ball.dy = 0.1

pen = turtle.Turtle()
pen.speed(0)
pen.color("white")
pen.penup()             #Första pennan... turtle.Turtle()
pen.hideturtle()
pen.goto(-200, 260)

pen2 = turtle.Turtle()
pen2.speed(0)
pen2.color("white")                 #andra pennan... turtle.Turtle()
pen2.penup()
pen2.penup()             
pen2.hideturtle()
pen2.goto(200, 260)

pen.hideturtle()
pen2.hideturtle()              
paddle_a.hideturtle()
paddle_b.hideturtle()
ball.hideturtle()

def start():
    startF.direction = "redo"
    penst.clear()
    penk.clear()
    pen.clear()
    pen2.clear()
    pen.goto(-200, 260)
    pen2.goto(200, 260)
    ball.showturtle()
    pen.write("player " + input + ": 0", align ="center", font=("courier", 24, "normal"))
    pen2.write("player " + input2 + ": 0", align ="center", font=("courier", 24, "normal")) 
    pen.showturtle()
    pen2.showturtle()              
    paddle_a.showturtle()
    paddle_b.showturtle()

    

input = wn.textinput("Wait","What would you like to name your self, player A?")
pen.goto(-290, -280)
pen.color("blue")
pen.write("player " + input , align ="center", font=("courier", 30, "normal"))

     

input2 = wn.textinput("Wait","What would you like to name your self, player b?")
pen2.goto(290, -280)
pen2.color("red")
pen2.write("player " + input2 , align ="center", font=("courier", 30, "normal"))
 




def paddle_a_up():
    y = paddle_a.ycor()
    y += 20                     
    paddle_a.sety(y)
                             #Hur långt ett "steg" ska tas up och ner från paddan a när knapptrycket w eller s trycks ner. Som seden ska skickas till skärmtolkaren
def paddle_a_down():
    y = paddle_a.ycor()
    y -= 20
    paddle_a.sety(y)
#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------                                         
def paddle_b_up():
    y = paddle_b.ycor()
    y += 20
    paddle_b.sety(y)
                             #Hur långt ett "steg" ska tas up ner från paddan b när knapptrycket upåtpil eller nedåtpil trycks ner. Som seden ska skickas till skärmtolkaren                      
def paddle_b_down():
    y = paddle_b.ycor()
    y -= 20
    paddle_b.sety(y)

wn.listen()
wn.onkeypress(paddle_a_up, "w")
wn.onkeypress(paddle_a_down, "s")       
wn.onkeypress(paddle_b_up, "Up")            #Skärmtolken tar imot paddan a och b för up ner på skärmen 
wn.onkeypress(paddle_b_down, "Down")
wn.onkeypress(start, "f")

while True:
    wn.update()


   

    if startF.direction == "inte redo":
        ball.goto(0, 0)
	paddle_a.goto(-360, 0)
        paddle_b.goto(360, 0)
        
        
    
    ball.setx(ball.xcor() + ball.dx)       #Hur vinkeln av bollen ska fungera     
    ball.sety(ball.ycor() + ball.dy)

#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
    if n == 0:
        ball.goto(0, 0)
        n = 1
        if n == 1:                                                                  
            ball.dx = 0.2 
            ball.dy = 0.2               #bollen saktades ner för att någon missade. Var för tufft för någon antar jag.
    
    if n == 4:
        ball.dx = 0.4
        ball.dy = 0.4
        print("SPEED UP")           #andra stadiet av farten på bollen
        n = 6

    if n == 10:
        ball.dx = 0.6
        ball.dy = 0.6            #tredje stadiet av farten på bollen
        print("SPEED UP")
        n = 12

    if n == 16:
        ball.dx = 0.8
        ball.dy = 0.8              #fjärde stadiet av farten på bollen
        print("SPEED UP")
        n = 18


    
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    if score_a == 1:
        pen.clear()
        pen.write("player " + input + ": 1", align ="center", font=("courier", 24, "normal"))           #när score_a får poäng för hen en 1

    if score_a == 2:
        pen.clear()
        pen.write("player " + input + ": 2", align ="center", font=("courier", 24, "normal"))       #när score_a får poäng för hen en 2

    if score_a == 3:
        pen.clear()
        pen.write("player " + input + ": 3", align ="center", font=("courier", 24, "normal"))       #när score_a får poäng för hen en 3

    if score_b == 1:
        pen2.clear()
        pen2.write("player " + input2 + ": 1", align ="center", font=("courier", 24, "normal"))         #när score_b får poäng för hen en 1

    if score_b == 2:
        pen2.clear()
        pen2.write("player " + input2 + ": 2", align ="center", font=("courier", 24, "normal"))             #när score_b får poäng för hen en 1

    if score_b == 3:
        pen2.clear()
        pen2.write("player " + input2 + ": 3", align ="center", font=("courier", 24, "normal"))                 #när score_b får poäng för hen en 1

#------------------------------------------------------------------------------------------------------------------------------------------------------------

    if ball.ycor() > 290:
        ball.sety(290)          #Hela if är när bollen nuddar kanten högst up
        ball.dy *= -1
  
    if ball.ycor() < -290:
        ball.sety(-290)            #Hela if är när bollen nuddar kanten längst ner
        ball.dy *= -1

#-----------------------------------------------------------------------------------------------------------------------------------------------------------------

    if (ball.xcor() > 340 and ball.xcor() < 350) and (ball.ycor() < paddle_b.ycor() + 40 and ball.ycor() > paddle_b.ycor() -50):
      ball.setx(340)
      ball.dx *= -1             #Hela if är när bollen nuddar paddan b
      n += 1
    
    if (ball.xcor() < -340 and ball.xcor() > -350) and (ball.ycor() < paddle_a.ycor() + 40 and ball.ycor() > paddle_a.ycor() -50):
      ball.setx(-340) 
      ball.dx *= -1         #Hela if är när bollen nuddar paddan a
      n += 1

#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    if ball.xcor() > 390:
        score_a += 1
        ball.goto(0, 0)         #Hela if är när spelare a får poäng. D.V.S att bollen nuddar kanten på höger sida
        ball.dx *= -1
        n = 0  

    if ball.xcor() < -390:
        score_b += 1
        ball.goto(0, 0)         #Hela if är när spelare b får poäng. D.V.S att bollen nuddar kanten på vänster sida
        ball.dx *= -1
        n = 0  
                         
#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
         

    if paddle_a.ycor() > 246:           
        paddle_a.sety(246)
                                #När paddan a är längst upp och ner                        
    if paddle_a.ycor() < -240:
        paddle_a.sety(-240)
                                
    if paddle_b.ycor() > 246:
        paddle_b.sety(246)
                                #När paddan b är längst upp och ner                           
    if paddle_b.ycor() < -240:
        paddle_b.sety(-240)

#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    if score_a == 3:
        score_a = 0
        score_b = 0
        wn.bgcolor("white")            
        pen.clear()                 #NU VINNER A
        pen.color("black")
        pen.goto(0, 0)
        pen.write("player " + input + " WON. Better luck luck next time, player " + input2 , align ="center", font=("courier", 15, "normal"))
        time.sleep(t)
        turtle.bye()
       
    if score_b == 3:
        score_a = 0             #NU VINNER B
        score_b = 0
        wn.bgcolor("white")         
        pen.clear()
        pen.color("black")
        pen.goto(0, 0)
        pen.write("player " + input2 + " WON. Better luck luck next time, player " + input , align ="center", font=("courier", 25, "normal"))
        time.sleep(t)
        turtle.bye()



